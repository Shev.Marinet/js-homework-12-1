const button = document.getElementById('draw');
const circleDiameter = document.createElement('input');
const circleColor =document.createElement('input');
const circleDraw =document.createElement('button');
const circle = document.createElement('div');
button.style.display = 'inline-block';
button.style.width ='150px';
button.style.height ='50px';
button.style.borderRadius = '10px';
circleDiameter.id ='diameter';
circleColor.id = 'color';
circleDiameter.placeholder = 'Enter the circle diameter in pixels';
circleColor.placeholder = 'Enter the color of the circle';
circleDiameter.style.width = '250px';
circleDiameter.style.marginRight = '20px';
circleColor.style.width = '250px';
circleDraw.id ='circle-draw';
circleDraw.style.display = 'block';
circleDraw.style.width ='150px';
circleDraw.style.height ='50px';
circleDraw.style.borderRadius = '10px';
circleDraw.style.marginTop = '20px';
circleDraw.innerText = 'Draw';
circle.style.borderRadius = '50%';
circle.style.marginTop = '20px';

function validate (element){
    if (element.className === 'warning') {
        element.className = '';
        document.body.removeChild(document.body.children[3]);
    }

    if (isNaN(+element.value)|| +element.value < 1) {
        element.classList.add('warning');
        const msgElem = document.createElement('p');
        msgElem.className = 'warning-text';
        msgElem.innerHTML = `It isn't a number`;
        document.body.insertBefore(msgElem, document.getElementById('circle-draw'));
    }
}

document.addEventListener('click', function (e) {
    if (e.target.id === 'draw') {
        button.parentElement.removeChild(button);
        document.body.appendChild(circleDiameter);
        document.body.appendChild(circleColor);
        document.body.appendChild(circleDraw);
    }

    if (e.target.id === 'circle-draw'){
        validate(document.getElementById('diameter'));
        const diameterValue = +document.getElementById('diameter').value;
        const colorValue = document.getElementById('color').value;
        circle.style.width = diameterValue + 'px';
        circle.style.height = diameterValue + 'px';
        circle.style.backgroundColor = colorValue;
        console.dir(circle);
    }
});